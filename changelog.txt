Practica1
Se han añadido comentarios en algunos archivos del src.
Se creo el directorio build en workspace

-Practica 2:
Se crea una rama nueva llamada practica2 y se implementa el movimiento de las raquetas, pelota y esferas.
También se implementa la función de disparo de las raquetas.
Se crean las funciones de rebote de las pelotas y los disparos, y estos ultimos para que cuando detecte
choque con la otra raqueta la bloquee y minimice su tamaño durante un tiempo concreto. 

-PRACTICA 3:
Se creo una rama nueva para esta práctica.
Se crearon dos nuevos ficheros, logger y bot.
El primero para crear la tuberia
El segundo para controlar la raqueta automaticamente
Añadimos el código necesario y las librerías para las tuberias y los nuevos ficheros.
Se crea por ultimo la etiqueta v1.3 que indica el final de la practica y se sube a bitbucket

-PRACTICA 4:
Se crea una rama para la practica 4.
Creamos los cliente.cpp y servidor.cpp, ademas de sus respectivos mundos tanto .cpp como .h
Borramos el Mundo.cpp y el tenis.cpp que teniamos agregado anteriormente
Modificamos el CMakeLists para añadir los ejecutables.
Se comunica el servidor con el cliente y se han mostrado sus datos.
Para finalizar, hemos logrado que el cliente recibe la tecla para poder enviarla al servidor
Por ultimo, se fusionan las ramas y se crea la etiqueta v1.4 para finalizar la practica

-PRACTICA 5:
Se crea rama 5 y se conectan cliente y servidor con sockets
